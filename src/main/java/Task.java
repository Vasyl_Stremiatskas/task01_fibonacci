
import java.util.Scanner;

/**
 * Task
 *
 * @version Version 1.0.0
 *
 * @author Vasyl Stremiatskas
 */

class Task {
    /** lower value of the interval */
    private int firstNumber = 1;
    /** higher value of the interval */
    private int lastNumber = 2;
    /** number of the Fibonacci numbers*/
    private int fibonacciSize = 2;

    /**
     * This function is used to enter the interval limits by the user
     */
    private void inputNumbers() {
        try {
            int num1;
            int num2;

            Scanner in = new Scanner(System.in);
            System.out.println("Please, enter 2 numbers in the range from 1 to 100: ");
            System.out.print("First number: ");
            num1 = in.nextInt();
            System.out.print("Second number: ");
            num2 = in.nextInt();
            if(num1 < 1 || num2 < 1 || num1 > 100 || num2 > 100 || num1 == num2) {
                System.out.println("Invalid input, try again");
                inputNumbers();
                return;
            }
            if(num1 > num2) {
                firstNumber = num2;
                lastNumber = num1;
            }
            else {
                firstNumber = num1;
                lastNumber = num2;
            }
        }
        catch(Exception e) {
            System.out.println("Error: " + e.toString());
            System.out.println("Please. try again");
            inputNumbers();
        }
    }

    /**
     * This function is used to enter the number of Fibonacci numbers by the user
     */
    private void inputFibonacciN() {
        try {
            int n;
            Scanner in = new Scanner(System.in);
            System.out.println("Please, enter the size of Fibonacci set: ");
            n = in.nextInt();
            fibonacciSize = n;
        }
        catch(Exception e) {
            System.out.println("Error: " + e.toString());
            System.out.println("Please. try again");
            inputFibonacciN();
        }
    }

    /**
     * Checks the number for oddity
     * @param num the number
     * @return true if odd
     */
    private boolean isOdd(int num) {
        return num % 2 == 1;
    }

    /**
     * The function outputs even and odd numbers from the interval
     * @param num1 the beginning of the interval
     * @param num2 the end of the interval
     */
    private void printOddAndEvenNum(int num1, int num2) {
        System.out.print("Odd numbers: ");
        for(int i = num1; i <= num2; i++) {
            if(isOdd(i)) {
                System.out.print(i);
                System.out.print(" ");
            }
        }

        System.out.println();
        System.out.print("Even numbers: ");
        for(int i = num2; i >= num1; i--) {
            if(!isOdd(i)) {
                System.out.print(i);
                System.out.print(" ");
            }
        }

        play();
    }

    /**
     * The function outputs the sum of even and odd numbers from the interval
     * @param num1 the beginning of the interval
     * @param num2 the end of the interval
     */
    private void printSumOddAndEvenNum(int num1, int num2) {
        int oddSum = 0;
        int evenSum = 0;

        System.out.println();
        System.out.print("Sum of odd numbers: ");
        for(int i = num1; i <= num2; i++) {
            if(isOdd(i)) {
                oddSum += i;
            }
        }
        System.out.print(oddSum);

        System.out.println();
        System.out.print("Sum of even numbers: ");
        for(int i = num2; i >= num1; i--) {
            if(!isOdd(i)) {
                evenSum += i;
            }
        }
        System.out.print(evenSum);

        play();
    }

    /**
     * The function outputs the largest even and odd numbers of Fibonacci numbers
     * @param n the number of Fibonacci numbers
     */
    private void buildFibonacci(int n) {
        int f1 = 1;
        int f2 = 1;
        int a = 1;
        int b = 1;
        int fibSum;
        for(int i = 0; i < n - 2; i++) {
            fibSum = a + b;
            a = b;
            b = fibSum;

            if(isOdd(fibSum) && fibSum > f1) {
                f1 = fibSum;
            }

            if(!isOdd(fibSum) && fibSum > f2) {
                f2 = fibSum;
            }
        }
        System.out.println("The biggest odd Fibonacci number (F1) is - " + String.valueOf(f1));
        System.out.println("The biggest even Fibonacci number (F2) is - " + String.valueOf(f2));

        play();
    }

    /**
     * The function calculates and outputs the percentage of even and odd numbers in a Fibonacci numbers
     * @param n the number of Fibonacci numbers
     */
    private void countOddEvenFibPerc(int n) {
        int oddCount = 2;
        int evenCount = 0;
        int f1 = 1;
        int f2 = 1;
        int fibSum;
        double oddPercent = 0.0;
        double evenPercent = 0.0;

        for(int i = 0; i < n - 2; i++) {
            fibSum = f1 + f2;
            f1 = f2;
            f2 = fibSum;

            if(isOdd(fibSum)) {
                oddCount++;
            }
            if(!isOdd(fibSum)) {
                evenCount++;
            }
            oddPercent = 100.0 * oddCount/(oddCount + evenCount);
            evenPercent = 100.0 * evenCount/(oddCount + evenCount);
        }
        System.out.print("The percentage of odd numbers in a number of Fibonacci numbers is: ");
        System.out.println(String.valueOf(oddPercent) + "%");
        System.out.print("The percentage of even numbers in a number of Fibonacci numbers is: ");
        System.out.println(String.valueOf(evenPercent) + "%");

        play();

    }

    /**
     * Function for dialogue with the user
     */
    void play() {
        try {
            int choice;
            Scanner in = new Scanner(System.in);

            System.out.println();
            System.out.println();
            System.out.println("Please, select an action: ");
            System.out.println("1 - program prints odd numbers from start to the end of interval and even from end to start");
            System.out.println("2 - program prints the sum of odd and even numbers");
            System.out.println("3 - program build Fibonacci numbers: F1 will be the biggest odd number and F2 – the biggest even number, user can enter the size of set (N)");
            System.out.println("4 - program prints percentage of odd and even Fibonacci numbers");
            System.out.println("0 - for exit");
            System.out.print("Your number: ");
            choice = in.nextInt();

            switch(choice) {
                case(0):  System.out.println("Goodbye.");
                    return;
                case(1):  inputNumbers();
                    printOddAndEvenNum(firstNumber, lastNumber);
                    break;
                case(2):  inputNumbers();
                    printSumOddAndEvenNum(firstNumber, lastNumber);
                    break;
                case(3):  inputFibonacciN();
                    buildFibonacci(fibonacciSize);
                    break;
                case(4):  inputFibonacciN();
                    countOddEvenFibPerc(fibonacciSize);
                    break;
                default:  System.out.println("Try again.");
                    play();
                    break;
            }
        }
        catch(Exception e) {
            System.out.println("Error: " + e.toString());
            System.out.println("Please. try again");
            play();
        }
    }
}




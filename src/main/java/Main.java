/**
 * Main
 *
 * @version Version 1.0.0
 *
 * @author Vasyl Stremiatskas
 */
public class Main {
    public static void main(String args[]) {
        Task t = new Task();
        t.play();
    }
}
